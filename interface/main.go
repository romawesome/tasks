package main

import "fmt"

type Payer interface {
	Pay(int) error
}

type Wallet struct {
	Cash int
}
type Card struct {
	Balance    int
	ValidUntil string
	Cardholder string
	CVV        string
	Number     string
}

type ApplePay struct {
	Money   int
	AppleID string
}

func (w *Wallet) Pay(amount int) error {
	if w.Cash < amount {
		return fmt.Errorf("Не хватает денег в кошельке")
	}
	w.Cash -= amount
	return nil
}

func (w *Card) Pay(amount int) error {
	if w.Balance < amount {
		return fmt.Errorf("Не хватает денег в кошельке")
	}
	w.Balance -= amount
	return nil
}

func (w *ApplePay) Pay(amount int) error {
	if w.Money < amount {
		return fmt.Errorf("Не хватает денег в кошельке")
	}
	w.Money -= amount
	return nil
}

func Buy(p Payer) {

	switch p.(type) {
	case *Wallet:
		fmt.Println("Оплата наличными?")
	case *Card:
		plasticCard, ok := p.(*Card)
		if !ok {
			fmt.Println("Не удалось преобразовать к типу *Card")
		}
		fmt.Println("Вставляйте карту, ", plasticCard.Cardholder)
	default:
		fmt.Println("Что то новое!")
	}

	err := p.Pay(10)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Спасибо за покупку через %T\n\n", p)
}

func main() {
	myWallet := &Wallet{Cash: 100}
	Buy(myWallet)

	var myMoney Payer

	myMoney = &Card{Balance: 100, Cardholder: "kuzmneko"}
	Buy(myMoney)

	myMoney = &ApplePay{Money: 10, AppleID: "123"}
	Buy(myMoney)
}

// -- встраивание интерфейса

type Payer2 interface {
	Pay(int) error
}

type Ringer interface {
	Ring(int) error
}
type NFCPhone interface {
	Payer2
}


