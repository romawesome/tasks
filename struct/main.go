package main

import (
	"fmt"
)

type Person struct {
	Id   int
	Name string
}

//
func (p Person) UpdateName(name string) {
	p.Name = name
}

//Persin
func (p *Account) SetName(name string) {
	p.Name = name
}

type Account struct {
	Id   int
	Name string
	Person
}

func main() {
	//pers := new(Person)
	//pers.SetName("Roman Kuzmmenko")
	//fmt.Printf("update person: %#v\n", pers)

	var acc Account = Account{
		Id:   1,
		Name: "kuzmrom7",
		Person: Person{
			Id:   2,
			Name: "Roman Kuzmenko",
		},
	}

	acc.SetName("Max Kuzmenko")
	acc.Person.SetName("Anatoly Kuzmenko")
	fmt.Printf("update person: %#v\n", acc)
}
